import 'package:festswipe/blocs/festival_bloc.dart';
import 'package:festswipe/connectors/festival_connector.dart';
import 'package:festswipe/pages/festival/complete/complete_page.dart';
import 'package:festswipe/pages/festival/index/index_page.dart';
import 'package:festswipe/pages/festival/view/view_page.dart';
import 'package:festswipe/pages/splash/splash_page.dart';
import 'package:festswipe/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => FestivalCubit(festivalConnector: FestivalConnector()),
      child: MaterialApp(
        title: 'FestSwipe',
        theme: AppTheme.lightTheme,
        initialRoute: Splash.route,
        routes: {
          Splash.route: (_) => Splash(
              festivalCubit:
                  FestivalCubit(festivalConnector: FestivalConnector())),
          FestivalIndex.route: (_) => const FestivalIndex(),
          FestivalView.route: (_) => const FestivalView(),
          FestivalComplete.route: (_) => const FestivalComplete(),
        },
      ),
    );
  }
}
