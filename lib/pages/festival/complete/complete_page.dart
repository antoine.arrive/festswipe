import 'package:festswipe/pages/festival/index/index_page.dart';
import 'package:flutter/material.dart';

import '../../../models/festival.dart';

class FestivalComplete extends StatelessWidget {
  static const route = '/festival/complete';
  const FestivalComplete({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Festival festival =
        ModalRoute.of(context)?.settings.arguments as Festival;

    return Scaffold(
        appBar: AppBar(
          title: Text(festival.title),
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 100),
          child: ListView(children: [
            ElevatedButton(
              onPressed: () => {
                Navigator.pushNamedAndRemoveUntil(
                    context, FestivalIndex.route, (route) => false)
              },
              child: const Center(
                child: Text('Go to home'),
              ),
            ),
            ElevatedButton(
              onPressed: () => {},
              child: const Center(
                child: Text('Générer le running order'),
              ),
            ),
            ElevatedButton(
              onPressed: () => {},
              child: const Center(
                child: Text('Générer une playlist'),
              ),
            )
          ]),
        ));
  }
}
