import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../models/festival.dart';

class FestivalCard extends StatelessWidget {
  final Festival festival;
  const FestivalCard({Key? key, required this.festival}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            leading: Image.network(festival.logo),
            title: Text(festival.title),
            subtitle: Text(
              '${festival.dateStart.day.toString()}/${festival.dateStart.month.toString()}/${festival.dateStart.year.toString()} - ${festival.dateEnd.day.toString()}/${festival.dateEnd.month.toString()}/${festival.dateEnd.year.toString()}',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Image.network(festival.image),
        ],
      ),
    );
  }
}
