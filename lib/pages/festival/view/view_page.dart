import 'package:festswipe/models/festival.dart';
import 'package:festswipe/pages/artists/components/artists_button.dart';
import 'package:festswipe/pages/festival/complete/complete_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:swipe_cards/swipe_cards.dart';

import '../../artists/components/artist_swipe.dart';

class FestivalView extends StatefulWidget {
  static const route = '/festival/view';
  const FestivalView({Key? key}) : super(key: key);

  @override
  State<FestivalView> createState() => _FestivalViewState();
}

class _FestivalViewState extends State<FestivalView> {
  late MatchEngine _matchEngine;

  @override
  Widget build(BuildContext context) {
    final Festival festival =
        ModalRoute.of(context)?.settings.arguments as Festival;

    _matchEngine = MatchEngine(
        swipeItems: festival.artists
            .map(
              (e) => SwipeItem(
                content: e.name,
                likeAction: () {
                  print("Like ${e.name}");
                },
                nopeAction: () {
                  print("Nope ${e.name}");
                },
                superlikeAction: () {
                  print("Superlike ${e.name}");
                },
              ),
            )
            .toList());

    return Scaffold(
      appBar: AppBar(
        title: Text(festival.title),
      ),
      body: Container(
          child: Column(children: [
        Container(
          height: 50,
        ),
        Expanded(
          child: SwipeCards(
            itemBuilder: (BuildContext context, int index) {
              // Affichage de l'artiste
              // https://pub.dev/packages/swipe_cards
              return ArtistSwipe(artist: festival.artists[index]);
            },
            onStackFinished: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, FestivalComplete.route, (route) => false,
                  arguments: festival);
            },
            matchEngine: _matchEngine,
          ),
        ),
        Container(
          height: 100,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ArtistButton(
                  action: () {
                    _matchEngine.currentItem?.like();
                  },
                  icon: Icons.thumb_down,
                  color: Colors.red),
              ArtistButton(
                  action: () {
                    _matchEngine.currentItem?.like();
                  },
                  icon: Icons.thumb_up,
                  color: Colors.green)
            ],
          ),
        )
      ])),
    );
  }
}
