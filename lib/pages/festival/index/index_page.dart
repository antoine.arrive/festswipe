import 'package:festswipe/blocs/festival_bloc.dart';
import 'package:festswipe/pages/festival/view/view_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../components/festival_card.dart';

class FestivalIndex extends StatelessWidget {
  static const route = '/festival';
  const FestivalIndex({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("FESTSWIPE"),
        ),
        body: Column(
          children: [
            BlocBuilder<FestivalCubit, FestivalBloc>(
              builder: (context, festivalBloc) => Expanded(
                child: ListView.builder(
                    itemCount: festivalBloc.festivals.length,
                    itemBuilder: (context, index) {
                      return Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(context, FestivalView.route,
                                  arguments: festivalBloc.festivals[index]);
                            },
                            child: FestivalCard(
                              festival: festivalBloc.festivals[index],
                            ),
                          ));
                    }),
              ),
            )
          ],
        ));
  }
}
