import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ArtistButton extends StatelessWidget {
  VoidCallback action;
  IconData icon;
  MaterialColor color;

  ArtistButton(
      {Key? key, required this.action, required this.icon, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        shape: const CircleBorder(),
        padding: const EdgeInsets.all(12),
      ),
      onPressed: () => action(),
      child: Icon(icon, color: color),
    );
  }
}
