import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../models/artist.dart';

class ArtistSwipe extends StatelessWidget {
  final Artist artist;
  const ArtistSwipe({Key? key, required this.artist}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: 'iLnmTe5Q2Qw',
      flags: YoutubePlayerFlags(
        autoPlay: true,
        mute: true,
      ),
    );

    return Stack(alignment: AlignmentDirectional.topCenter, children: [
      Image.network(artist.image, fit: BoxFit.cover),
      Padding(
        padding: const EdgeInsets.only(top: 50),
        child: Text(
          artist.name,
          style: const TextStyle(color: Colors.white, fontSize: 40, shadows: [
            Shadow(color: Colors.black, blurRadius: 1, offset: Offset(2, 2))
          ]),
        ),
      ),
    ]);
  }
}
