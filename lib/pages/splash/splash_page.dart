import 'package:festswipe/blocs/festival_bloc.dart';
import 'package:festswipe/pages/festival/index/index_page.dart';
import 'package:festswipe/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Splash extends StatelessWidget {
  static const route = '/';

  FestivalCubit festivalCubit;

  Splash({Key? key, required this.festivalCubit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void redirect() async {
      // Récupération des festivals
      context.read<FestivalCubit>().getAllFestival();
      await Future.delayed(const Duration(seconds: 2));
      Navigator.pushNamedAndRemoveUntil(
          context, FestivalIndex.route, (route) => false);
    }

    redirect();

    return Scaffold(
      body: Container(
        color: AppTheme.mainBackground,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('FestSwipe', style: TextStyle(fontSize: 50)),
          ],
        )),
      ),
    );
  }
}
