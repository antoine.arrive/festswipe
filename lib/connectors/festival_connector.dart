import 'package:festswipe/models/artist.dart';
import 'package:festswipe/models/festival.dart';

class FestivalConnector {
  static Artist gojira = Artist(
      name: 'Gojira',
      image:
          'https://www.telerama.fr/sites/tr_master/files/styles/simplecrop1000/public/gojira_2020_0.jpg?itok=1u7Qhuaq');
  static Artist alcest = Artist(
      name: 'Alcest',
      image:
          'https://cdn-s-www.dna.fr/images/E7A667D7-F1AC-4857-9211-F4090DEDF337/NW_detail_M/title-1576680650.jpg');
  static Artist architects = Artist(
      name: 'Architects',
      image:
          'https://rocknfolk-cdn.respawn.fr/wp-content/uploads/rf-medias/RF_618_2019_01_19/press_package/files/images/image_0026_0032.jpg');
  static Artist crasyfists = Artist(
      name: '36 Crazyfists',
      image:
          'https://metalanarchydotcom.files.wordpress.com/2017/12/fb_img_1513369667177.jpg');

  Festival hellfest = Festival(
      id: '1',
      title: 'Hellfest 2022',
      subTitle: 'Rendez-vous en enfer',
      image:
          'https://lifestyle.oblikon.net/wp-content/uploads/sites/3/2022/03/hellfest2022.jpg',
      logo: 'https://corsairesdenantes.fr/wp-content/uploads/2019/04/hellfest.png',
      dateStart: DateTime(2022, 6, 17),
      dateEnd: DateTime(2022, 6, 26),
      artists: [gojira, architects, alcest, crasyfists]);

  Festival motocultor = Festival(
      id: '2',
      title: 'Motocultor',
      subTitle: '',
      image:
          'https://www.motocultor-festival.com/wp-content/uploads/2021/11/ANNONCE-motocultor-2022.-724x1024.jpg',
      logo:
          'https://www.motocultor-festival.com/wp-content/uploads/2021/11/cropped-logomotocultor2022.png',
      dateStart: DateTime(2022, 8, 18),
      dateEnd: DateTime(2022, 8, 21),
      artists: []);

  Future<List<Festival>> getAll() async {
    await Future.delayed(const Duration(seconds: 1));
    return Future.value([hellfest, motocultor]);
  }

  Future<Festival> getById(String id) async {
    await Future.delayed(const Duration(seconds: 1));
    return Future.value((id == '2') ? motocultor : hellfest);
  }
}
