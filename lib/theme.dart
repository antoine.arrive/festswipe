import 'dart:ui';

import 'package:flutter/material.dart';

class AppTheme {
  static const Color mainBackground = Colors.white;
  static final lightTheme = ThemeData(
      appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white,
          centerTitle: true,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          titleTextStyle: TextStyle(color: Colors.black, fontSize: 25)));
}
