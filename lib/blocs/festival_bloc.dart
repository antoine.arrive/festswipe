import 'package:festswipe/connectors/festival_connector.dart';
import 'package:festswipe/models/festival.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../models/state.dart';

class FestivalBloc {
  List<Festival> festivals;
  State state;

  FestivalBloc({required this.festivals, required this.state});
}

class FestivalCubit extends Cubit<FestivalBloc> {
  FestivalConnector festivalConnector;

  FestivalCubit({required this.festivalConnector})
      : super(FestivalBloc(festivals: [], state: State.init));

  void getAllFestival() async {
    emit(FestivalBloc(festivals: state.festivals, state: State.pending));

    List<Festival> festivals = await festivalConnector.getAll();

    emit(FestivalBloc(festivals: festivals, state: State.success));
  }
}
