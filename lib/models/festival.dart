import 'package:festswipe/models/artist.dart';

class Festival {
  final String id;
  final String title;
  final String? subTitle;
  final String image;
  final String logo;
  final DateTime dateStart;
  final DateTime dateEnd;
  final List<Artist> artists;

  Festival(
      {required this.id,
      required this.title,
      this.subTitle,
      required this.image,
      required this.logo,
      required this.dateStart,
      required this.dateEnd,
      required this.artists});

  @override
  String toString() {
    return title;
  }
}
